package ez.lingo.hig.no.ez_wakup;

import android.location.Location;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class contains methods for extracting weather information from OpenWeatherMap.
 * It communicates with NewAlarm for ensuring that weather data is only requested when
 * it actually has finished downloading. See the constructor for more details.
 * @see #WeatherData(AlarmReceiver, String, Location)
 * @author Per-Kristian Nilsen
 * Date: 20.09.2015
 */


public class WeatherData {

    private InputStream inputStream;
    private String jsonAsString;
    private final String TAG = "";

    /**
     * This constructor is to be called from the AlarmReceiver activity. It downloads weather data
     * from OpenWeatherMap.org in a separate thread. When the thread has finished downloading
     * the data and saved it in jsonAsString, AlarmReceiver's readyGetWeather() is called,
     * which further calls WeatherData's getCurrentWeatherName(). That way the "name"
     * of the weather can be saved and used in AlarmReceiver.
     * @param callerActivity instance of the calling AlarmReceiver activity
     * @param cityName name of the city, something like "Gjovik,no"
     * @param location user location object, used for getting lat/lng if Geocoder fails"
     */
    public WeatherData(final AlarmReceiver callerActivity,
                       final String cityName, final Location location){
        Thread thread = new Thread(){
            public void run(){
                try {
                    String urlString;

                    // If the Geocoder works, use cityName for getting weather. Else use
                    // lat/lng from location.
                    if (location == null)
                        urlString = "http://api.openweathermap.org/data/2.5/weather?q=" +
                            cityName + "&APPID=dda604e24caf4e8228923464f48a5cb2";
                    else
                        urlString = "http://api.openweathermap.org/data/2.5/weather?lat="+
                                location.getLatitude() +"&lon=" + location.getLongitude() +
                                "&APPID=dda604e24caf4e8228923464f48a5cb2";

                    // Set up the connection to the web resource
                    URL weatherURL = new URL(urlString);
                    HttpURLConnection connection =
                            (HttpURLConnection) weatherURL.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.connect();

                    // Read the input stream, save it as a string
                    inputStream = connection.getInputStream();
                    jsonAsString = readStream(inputStream);

                    // Tell AlarmReceiver that the thread has finished downloading the data.
                    callerActivity.readyGetWeather(true);

                    if (inputStream != null) {
                        inputStream.close();
                    }

                } catch (MalformedURLException malex){
                    Log.e(TAG, "Something went wrong with the URL.");
                    malex.printStackTrace();
                    callerActivity.readyGetWeather(false);

                } catch (IOException ioex){
                    Log.e(TAG, "IOException. Possibly the http connection.");
                    ioex.printStackTrace();
                    callerActivity.readyGetWeather(false);
                }
            }
        };
        thread.start();
    }


    /**
     * This method is to be used only when weatherdata already has been downloaded.
     * Since that is done in a separate thread (see constructor), make SURE that the thread has
     * finished running before calling this function. See the constructor's JavaDoc comments
     * for more information.
     * @see #WeatherData(AlarmReceiver, String, Location)
     * @return type of weather
     */
    public String getCurrentWeatherName(){

        try{
            JSONObject completeObject = new JSONObject(jsonAsString);
            JSONArray weatherArray = new JSONArray();
            weatherArray.put(completeObject.getJSONArray("weather"));

            //Must make JSONObject of the JSONArray
            JSONArray innerWeatherArray = weatherArray.getJSONArray(0);
            JSONObject weatherObject = innerWeatherArray.getJSONObject(0);

            return weatherObject.getString("main");
        } catch (JSONException jsonex){
            Log.e(TAG, "JSONException in getCurrentWeatherCollection");
            jsonex.printStackTrace();
        }
        return null;
    }

    /**
     * This method handles reading of input streams.
     * @param inputStream the input stream to be read
     * @return a string containing the data from the input stream
     */
    private String readStream(InputStream inputStream) {
        try {
            Reader reader = new InputStreamReader(inputStream);
            char[] temp = new char[800];
            reader.read(temp);
            return new String(temp);

        } catch (IOException ioex) {
            Log.e(TAG, "Something went wrong in readStream.");
            ioex.printStackTrace();
        }
        return null;
    }
}
