package ez.lingo.hig.no.ez_wakup;

/**
 * Constants used in the GeoCoder service for passing results and package name and so on
 * @since 25/09/15
 * @author Jardar Tøn
 */
public class ConstGeocoder {
           public static final int SUCCESS_RESULT = 0;
        public static final int FAILURE_RESULT = 1;
        public static final String PACKAGE_NAME =
                "ez.lingo.hig.no.ez_wakup";
        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
        public static final String RESULT_DATA_KEY = PACKAGE_NAME +
                ".RESULT_DATA_KEY";
        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
                ".LOCATION_DATA_EXTRA";
}
