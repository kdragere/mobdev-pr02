package ez.lingo.hig.no.ez_wakup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * The main activity of the app. This displays all the alarms and contains a button for
 * launching the newAlarm activity.
 * @author Kristian Dragerengen, Jardar Tøn
 * @since 19/09/15
 */
public class MainActivity extends AppCompatActivity {
    private int alarmNumber;
    private final int MAXALARMS = 6; //CONST
    private final int[] alarmHours = new int[MAXALARMS];
    private final int[] alarmMinutes = new int[MAXALARMS];
    private final Button[] alarm = new Button[MAXALARMS];
    private DeleteAlarm deleteAlarm;
    private SharedPreferences sharedPreferences;
    private LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        android.support.v7.app.ActionBar menu = getSupportActionBar();
        menu.setDisplayShowHomeEnabled(true);
        menu.setLogo(R.mipmap.icon);
        menu.setDisplayUseLogoEnabled(true);
        loadAlarms();
        checkGooglePlayServices();
    }

    /**
     * newAlarm starts a new activity with the parameter alarmNumber
     */
    public void newAlarm(final View v) {
        Button newAlarm = (Button) findViewById(R.id.newAlarm);
        final Intent i = new Intent(this, NewAlarm.class);
        i.putExtra("alarmNumber", alarmNumber);
        startActivity(i);
    }

    /**
    * Checks to see if user has google play services.
    * If not a dialog appears prompting them to install it.
    */
    private boolean checkGooglePlayServices() {
        final int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            Log.e("GgooglePS", GooglePlayServicesUtil.getErrorString(status));

            // ask user to update google play services.
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, 1);
            dialog.show();
            return false;
        } else {
            Log.i("googlePS", GooglePlayServicesUtil.getErrorString(status));
            return true;
        }
    }

    /**
     * OnclickListener gives the user a message that he or she has ut hold the button.
     */
    private final View.OnClickListener push = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getBaseContext(), getString(R.string.holdbutton), Toast.LENGTH_SHORT).show();
        }
    };

    /**
     * OnLongClickListerner opens a menu where an alarm can be deleted or configured.
     * The delete function opens a new delete pbject according the the alert. The change
     * function starts a new alarm activity.
     */
    private final View.OnLongClickListener hold = new View.OnLongClickListener() {
        int id;
        @Override

        // Adda popupmenu for longclick with options for delete and change
        public boolean onLongClick(View v) {
            id=v.getId();
            mainLayout = (LinearLayout) findViewById(R.id.innerLayout);
            PopupMenu popup = new PopupMenu(MainActivity.this, alarm[id]); // SETT VARIABLE
            popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.delete:
                                    deleteAlarm = new DeleteAlarm(id, MainActivity.this, mainLayout);
                                    alarmNumber--;
                            return true;
                        case R.id.change:
                            final Intent i = new Intent(MainActivity.this, NewAlarm.class);
                            i.putExtra("alarmNumber", id);
                            startActivity(i);
                            return true;
                    }
                   return true;
                }
            });
            popup.show();
            return true;
        }
    };

    /**
     * This function hour and minute from shared preferences and writes them
     * to screen. They are displayed as buttons. If the date format is set to
     * 12 hours it will display time as AM \ PM.
     *  Ther are som format issues with hours and minutes which starts with 0,
     *  therefore the function will add a 0 in front of all the times that starts with 0
     */
    private void loadAlarms() {
        alarmNumber = 1;
        int restoredText;
        int loadedHour = -1;
        int loadedMinute = -1;

        //Creates a new layout and get data from shared prefs
        mainLayout = (LinearLayout) findViewById(R.id.innerLayout);
        sharedPreferences = this.getSharedPreferences("ez.lingo.hig.no.ez_wakup", Context.MODE_PRIVATE);
        for (int i = 1; i <= MAXALARMS; i++) {
            restoredText = sharedPreferences.getInt("alarm"+i+"h", -1);
            if (restoredText != -1) {
                Log.d("for loop int", String.valueOf(i));
                loadedHour = sharedPreferences.getInt("alarm" + i + "h", -1);
                loadedMinute = sharedPreferences.getInt("alarm" + i + "m", -1);

                String format = "";

                // Change clock format if 12h format
                if(!DateFormat.is24HourFormat(getApplicationContext())) {
                    if (loadedHour == 0) {
                        loadedHour += 12;
                        format = " AM";
                    } else if (loadedHour == 12) {
                        format = " PM";
                    } else if (loadedHour > 12) {
                        loadedHour -= 12;
                        format = " PM";
                    } else {
                        format = " AM";
                    }
                }

                // Adds an extra 0 in front of some times, because og format error
                StringBuilder timeString = new StringBuilder();

                if (loadedHour < 10) {
                    timeString.append("0").append(loadedHour).append(" : ");
                }
                else
                    timeString.append(loadedHour).append(" : ");

                if (loadedMinute < 10) {
                    timeString.append("0").append(loadedMinute).append(format);
                }
                else
                    timeString.append(loadedMinute).append(format);

                // Adds alarm to a button and enable click listeners
                alarmHours[i] = loadedHour;
                alarmMinutes[i] = loadedMinute;
                alarm[i] = new Button(this); //initialize the button here
                mainLayout.addView(alarm[i]);
                //noinspection ResourceType
                alarm[i].setId(i); // Sets ID like alarm number
                alarm[i].setHeight(200);
                alarm[i].setHighlightColor(324945);
                alarm[i].setText(timeString);
                alarm[i].setOnClickListener(push);
                alarm[i].setOnLongClickListener(hold);
                alarmNumber++;
            }
        }
    }
}
