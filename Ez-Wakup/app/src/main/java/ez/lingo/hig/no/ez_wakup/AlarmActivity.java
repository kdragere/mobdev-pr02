package ez.lingo.hig.no.ez_wakup;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This activity handles the interaction with the user when the alarm goes off. It uses the
 * WakeLock functionality to be able to show the alarm activity even if the device is locked.
 * <a href="http://www.steventrigg.com/alarm-screen-wakelock-and-mediaplayer-create-an-alarm-clock-in-android-tutorial-part-7/">
 * This tutorial</a> inspired us to use this solution.
 * @author Per-Kristian Nilsen
 * Date: 24.09.2015
 */
public class AlarmActivity extends AppCompatActivity {

    private PowerManager.WakeLock wakeLock;
    private MediaPlayer player;

    /**
     * This function is basically called when the alarm goes off. It is mainly used for managing
     * the WakeLock functionality.
     */
    @Override
    protected void onResume(){
        super.onResume();

        // Set the window to keep screen on
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        // Get a hold of a wakelock
        PowerManager powerManager =
                (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);

        if (wakeLock == null) {
            wakeLock = powerManager.newWakeLock((PowerManager.FULL_WAKE_LOCK |
                    PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                    PowerManager.ACQUIRE_CAUSES_WAKEUP), this.getClass().getSimpleName());
        }

        if (!wakeLock.isHeld()){
            wakeLock.acquire();
        }
    }

    /**
     * This function releases the wakelock resource when the activity is paused.
     */
    @Override
    protected void onPause(){
        super.onPause();

        if (wakeLock != null && wakeLock.isHeld()){
            wakeLock.release();
        }
    }

    /**
     * This method is the first method to be called when the alarm goes off and this activity
     * is launched. It sets up the user interface and releases the wakelock after 2 minutes,
     * in case the user isn't nearby. This stops the alarm from draining the battery.
     * @param savedInstanceState saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        // Set up action bar
        android.support.v7.app.ActionBar menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayShowHomeEnabled(true);
            menu.setLogo(R.mipmap.icon);
            menu.setDisplayUseLogoEnabled(true);
        }
        // Get time and message from intent
        String message = getIntent().getStringExtra("MESSAGE");
        String time = getIntent().getStringExtra("TIME");

        // Display weather icon and time
        ImageView weatherDisplay = (ImageView) findViewById(R.id.textAlarmMessage);

        TextView textViewTime = (TextView) findViewById(R.id.textTime);
        textViewTime.setText(time);

        // Display dismiss button
        Button buttonDismiss = (Button) findViewById(R.id.buttonDismiss);

        buttonDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Clicking the player stops the music and exits the application
                player.stop();
                System.exit(1);
            }
        });

        try {

            Uri customToneUri;
            // Get path ready for selecting audio files
            String path = "android.resource://" + this.getPackageName() + "/";

            // Play appropriate sounds depending on the weather
            switch (message){
                // Both "Clear" and "Clouds" use the same ringtone
                case "Clear":
                case "Clouds":
                    customToneUri = Uri.parse(path + R.raw.clouds);
                    playRingtone(customToneUri);
                    weatherDisplay.setImageResource(R.drawable.cloud);
                    break;

                case "Thunderstorm":
                    customToneUri = Uri.parse(path + R.raw.thunder);
                    playRingtone(customToneUri);
                    weatherDisplay.setImageResource(R.drawable.storm);
                    break;

                // Both drizzle and Rain use the same ringtone
                case "Drizzle":
                case "Rain":
                    customToneUri = Uri.parse(path + R.raw.rain);
                    playRingtone(customToneUri);
                    weatherDisplay.setImageResource(R.drawable.rain);
                    break;

                case "Atmosphere":
                    customToneUri = Uri.parse(path + R.raw.crows);
                    playRingtone(customToneUri);
                    weatherDisplay.setImageResource(R.drawable.fair);
                    break;

                case "Snow":
                    customToneUri = Uri.parse(path + R.raw.sleighbells);
                    playRingtone(customToneUri);
                    weatherDisplay.setImageResource(R.drawable.snow);
                    break;

                // If nothing above is found, it uses the default alarm
                default:
                    Uri defaultToneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                    playRingtone(defaultToneUri);
                    weatherDisplay.setImageResource(R.drawable.na);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * This Runnable is used for releasing the wakelock resource. The reason for using a
         * Runnable is that it lets us use postDelayed from the Handler class, which again lets us
         * run this object after a certain time period.
         */
        Runnable releaseWakelock = new Runnable() {

            @Override
            public void run() {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

                if (wakeLock != null && wakeLock.isHeld()) {
                    wakeLock.release();
                    player.stop();
                    finish();
                }
            }
        };

        new Handler().postDelayed(releaseWakelock, 120 * 1000);
    }

    /**
     * This method plays the audio file specified in the parameter.
     * @param uri the uri of the audio file
     */
    private void playRingtone(Uri uri){
        player = MediaPlayer.create(this, uri);
        player.setLooping(true);
        player.start();
    }
}
