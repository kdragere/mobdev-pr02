package ez.lingo.hig.no.ez_wakup;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * This class handles all of location functionality in the app. It connects to a googleApi and
 * you can get the last location or start listening for updates.
 * <a href="https://developer.android.com/training/location/index.html">
 *     This tutorial </a> inspired us to use this solution.
 * @since 19/09/15
 * @author Jardar Ton1
 */
public class GetLocation implements ConnectionCallbacks,
             OnConnectionFailedListener, LocationListener {

    private AlarmReceiver callingAct;
    private Context context;
    private String mAddress;
    private Location mLocation;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    /**
     * sets up the google api client
     * @param caller reference to the calling class for calling back when done.
     * @param con Context of the activity.
     */
    public GetLocation(AlarmReceiver caller, Context con) {
        this.callingAct = caller;
        this.context = con;
    }

    /**
     * sets up the google api client
     */
    protected synchronized void buildGoogleApiClient () {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * checks if the api client was sucessfully set up and
     * tries to connect to it.
     */
    public void connectPlayServices() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    /**
     * When api client is connected the function tries to find the last location as a default.
     * It then starts the location listener listening for new updates.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        startLocationUpdates();
    }

    /**
     * This runs when the location listener has found a location update on the device.
     * it stores the location and stops the locationListener. Then lets the calling
     * activity know that a location has been found.
     */
    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        stopLocationUpdates();
        callingAct.readyLocation();
    }

    /**
     * Function that runs if the connection to the google api has failed
     */
    @Override
    public void onConnectionFailed (ConnectionResult result) {
        Log.d("GetLocation", "onConnectionFailed()called");
    }

    /**
     * Function that runs if the connection to the google api is broken
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d("GetLocation", "onConnectionSuspended() called. Trying to reconnect.");
    }

    /**
     * Creates a location request via createLocationRequest then it starts a Listener that
     * listens for updates on the location of the device.
     */
    private void startLocationUpdates() {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Creates a locationRequest with options on accuracy, update interval and so on.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * This function Stops the location listener.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    /**
     * Called from the calling class when this class has called
     * <b>readyLocation</b> of the calling class.
     * @see AlarmReceiver
     * @return location of the device
     */
    public Location getLocation() {
        return mLocation;
    }
}
