package ez.lingo.hig.no.ez_wakup;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Class is used as a service for getting city and country of a location.
 * <a href="https://developer.android.com/training/location/display-address.html" This
 * guide was using in creating this class.</a>
 * @since 19/09/15
 * @author Jardar Tøn
 */

public class FetchAddressIntentService extends IntentService {

    protected ResultReceiver mReceiver;

    public FetchAddressIntentService() {
        super("FetchAddressIntentService");
    }

    /**
     * Handles the intent. Gets the receiver and location. Checks the receiver to see that its not
     * null. If not it tries to reverse geocode. If successful calls deliverResultToReceiver
     * with a string containing The city name, comma country code for getting weather data.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        Location location = intent.getParcelableExtra(
                ConstGeocoder.LOCATION_DATA_EXTRA);

        mReceiver = intent.getParcelableExtra(
                ConstGeocoder.RECEIVER);

        if (mReceiver == null) {
            Log.wtf("RECIVER", "No receiver received. There is nowhere to send the results.");
            return;
        }

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            Log.e("revGeocoding", "serive not available", ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            Log.e("revGeocoding", "invalid lat long used", illegalArgumentException);
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
            deliverResultToReceiver(ConstGeocoder.FAILURE_RESULT, "0");
        } else {
            Address address = addresses.get(0);
            Log.i("revGeocoding", "adresses found");
            String cityCountry = address.getLocality().concat("," + address.getCountryCode());
            deliverResultToReceiver(ConstGeocoder.SUCCESS_RESULT,
                            cityCountry);
        }
    }

    /**
     * Returns the result of the geocoding back to the Receiver in a bundle.
     * @see ez.lingo.hig.no.ez_wakup.AlarmReceiver.AddressResultReceiver
     */
    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstGeocoder.RESULT_DATA_KEY, message);
        mReceiver.send(resultCode, bundle);
    }
}