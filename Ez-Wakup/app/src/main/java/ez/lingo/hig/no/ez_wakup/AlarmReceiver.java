package ez.lingo.hig.no.ez_wakup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import java.util.Date;

/**
 * This class is used for receiving the alarm broadcast and launching the alarm activity.
 * @since 25/09/15
 * @author Per-Kristian Nilsen, Jardar Tøn
 */
public class AlarmReceiver extends BroadcastReceiver {

    private Context contxt;
    private GetLocation location;
    private WeatherData weatherData;
    private AddressResultReceiver mResultReceiver;
    private String weatherDesc = "default test";
    private Location userLocation;

    /**
     * When the alarmBroadcast is received this function is called.
     * It Deletes the alarm that just went off from sharedPrefs.
     * Then it starts a googleApi to get the users location.
     */
    @Override
    public void onReceive(final Context context, Intent intent)
    {
        contxt = context;

        int alarmId = intent.getIntExtra("alarmId", 0);

        new DeleteAlarm(alarmId, context, null);

        location = new GetLocation(AlarmReceiver.this, contxt);
        location.buildGoogleApiClient();
        location.connectPlayServices();
        mResultReceiver = new AddressResultReceiver(new Handler());

        Log.d("alarm", "ALARM RECEIVED");
    }

    /**
     * Starts an alarmActivity.
     * Gets the current time, adds zeroes to display correctly as a time.
     * Starts a new intent providing it current weather and current time.
     * Then launches the alarmActivity.
     */
    private void startAlarm() {

        Date date = new Date(System.currentTimeMillis());

        String hour;
        String min;

        if (date.getHours() < 10)
           hour = "0" + String.valueOf(date.getHours());
        else
            hour = String.valueOf(date.getHours());

        if (date.getMinutes() < 10)
            min = "0" + String.valueOf(date.getMinutes());
        else
            min = String.valueOf(date.getMinutes());

        Intent alarmIntent = new Intent(contxt, AlarmActivity.class);
        alarmIntent.putExtra("MESSAGE", weatherDesc);
        alarmIntent.putExtra("TIME", hour+":"+min);

        alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        contxt.startActivity(alarmIntent);
    }

    /**
     * Saves the location of the device.
     * Then calls the function which starts the geocoding service.
     */
    public void readyLocation() {
        userLocation = location.getLocation();
        startIntentService(userLocation);
    }

    /**
     * Function gets called when the weatherData object has gotten/failed
     * to fetch the weather. Then it asks for the string containing the description of the weather.
     * Then the startAlarm function gets called.
     * @param foundWeather true or false on weather the data has been found or not
     */
    public void readyGetWeather(boolean foundWeather) {
        if (foundWeather)
            weatherDesc  = weatherData.getCurrentWeatherName();
        else
            weatherDesc = "no weather found";
        startAlarm();
    }

    /**
     * starts the geoCoding intentService.
     * @param location the current location of the device sent to the geoCoder service.
     */
    private void startIntentService(Location location) {
        Intent intent = new Intent(contxt, FetchAddressIntentService.class);
        intent.putExtra(ConstGeocoder.RECEIVER, mResultReceiver);
        intent.putExtra(ConstGeocoder.LOCATION_DATA_EXTRA, location);
        contxt.startService(intent);
    }


    /**
     * Receiver class witch gets the results from geoCoding service.
     */
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives the res from geoCoding service.
         * Then calls the weatherData class and providing a city name.
         * If the geoCoder fails it will use the location object instead.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            String mAddress = resultData.getString(ConstGeocoder.RESULT_DATA_KEY);
            if (mAddress != null) {
                if (mAddress.equals("0")) {
                    weatherData = new WeatherData(AlarmReceiver.this, mAddress, userLocation);
                } else weatherData = new WeatherData(AlarmReceiver.this, mAddress, null);
            }
        }
    }
}
