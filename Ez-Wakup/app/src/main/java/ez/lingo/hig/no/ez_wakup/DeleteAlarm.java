package ez.lingo.hig.no.ez_wakup;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.LinearLayout;

/**
 * This class deletes an alarm from shared preferences and unset the alarm itself.
 * Also a mechanism for changing an alarm.
 *
 * @since 25/09/15
 * @author Kristian Dragerengen
 */
public class DeleteAlarm {
    private final int MAXALARMS = 6;
    private int hour;
    private int minute;
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    public DeleteAlarm (final int alarmId, Context context, LinearLayout layout) {

        // Removes the alarm from shared prefs
        sharedPreferences = context.getSharedPreferences("ez.lingo.hig.no.ez_wakup", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove("alarm"+alarmId+"h");
        editor.remove("alarm" + alarmId + "m");
        editor.apply();


        // if layout eq null the alarm is not getting deleted just changed
        if (layout != null) {
            for (int i = alarmId; i < MAXALARMS; i++) {
                int j = i + 1;
                hour = sharedPreferences.getInt("alarm" + j + "h", -1);
                if (hour != -1) {
                    minute = sharedPreferences.getInt("alarm" + j + "m", -1);
                    editor.putInt("alarm" + i + "h", hour);
                    editor.putInt("alarm" + i + "m", minute);
                    editor.apply();
                } else {
                    editor.remove("alarm" + i + "h");
                    editor.remove("alarm" + i + "m");
                    editor.apply();
                }
            }
        }

        // Reset the alarm
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, alarmId, intent, 0);

        alarmManager.cancel(alarmIntent);

        // delete the alarm button if a view has been received.
        if (layout != null)
            layout.removeView(layout.findViewById(alarmId));
    }


}
