package ez.lingo.hig.no.ez_wakup;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.Calendar;
import java.util.Date;

/**
 * Activity is responsible for setting a new alarm. That means creating a pendingIntent with
 * alarmManger and saving the time to sharedPreferances.
 * The activity also shows the UI for the timePicker and button to set the alarm.
 * When a user changes a alarm this is also used as it just creates a new and deletes the old.
 * @since 19/09/15
 * @author Kristian Dragerengen, Jardar Tøn
 */
public class NewAlarm extends AppCompatActivity {
    private TimePicker timePicker;
    private int alarmNumber;
    private final int MAXALARMS = 6; //CONST

    private int hour;
    private int minute;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    /**
     * onCreate sets the App icon and loads the activity.
     * Saves the alarmNumber to an variable.
     * Sets the format of the timePicker depending on clock settings.
     * It also initializes the timePicker with the current time.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_alarm);
        android.support.v7.app.ActionBar menu = getSupportActionBar();
        menu.setDisplayShowHomeEnabled(true);
        menu.setLogo(R.mipmap.icon);
        menu.setDisplayUseLogoEnabled(true);

        Intent intent = getIntent();
        alarmNumber = intent.getIntExtra("alarmNumber", 0);

        timePicker = (TimePicker) findViewById(R.id.timePicker);
        if(DateFormat.is24HourFormat(getApplicationContext())) {
            timePicker.setIs24HourView(true);
        }

        Date date = new Date(System.currentTimeMillis());

        timePicker.setCurrentHour(date.getHours());
        timePicker.setCurrentMinute(date.getMinutes());
    }

    /**
     * Button that returns to MainActivity
     */
    public void back(final View v) {
        Button back = (Button) findViewById(R.id.back);
        final Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    /**
     * Sets a new alarm using alarmManager and then launches mainActivity.
     * It deletes an existing alarm if one with the same id exists. (used when changing an alarm)
     * Gets the time from the timePicker and uses it to set an alarm.
     * The intent in the alarmIntent has the id of the alarm. (to delete it when it goes off)
     * Then calls constructAlarmList to save the alarm in sharedPrefs. Then goes back to mainAct.
     */
    public void setAlarm(final View v) {

        DeleteAlarm delExisting = new DeleteAlarm(alarmNumber, this, null);

        hour = timePicker.getCurrentHour();
        minute = timePicker.getCurrentMinute();

        Calendar cal=Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        Date date = new Date(System.currentTimeMillis());

        cal.set(Calendar.MONTH, date.getMonth());
        cal.set(Calendar.YEAR, date.getYear() + 1900);
        cal.set(Calendar.DAY_OF_MONTH, date.getDate());
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);

        if (hour < date.getHours() || (hour == date.getHours() && minute < date.getMinutes()))
            cal.add(Calendar.DATE, 1);

        AlarmManager alarmManager = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        Intent intent= new Intent(this, AlarmReceiver.class);
        intent.putExtra("alarmId", alarmNumber);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(),
                alarmNumber, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), alarmIntent);

        constructAlarmList();
        final Intent y = new Intent(this, MainActivity.class);
        startActivity(y);
    }

    /**
     * This function inputs the hour and minute set by the user to shared preferences.
     * So this can be reloaded to the app if the user close it. It also displays a message if
     * the user has set a number of alarms == MAXALARMS
     */
    private void constructAlarmList() {
        sharedPreferences = this.getSharedPreferences("ez.lingo.hig.no.ez_wakup", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if (hour != -1 && minute != -1 && alarmNumber <= MAXALARMS-1 ) {
                editor.putInt("alarm"+alarmNumber+"h", hour);
                editor.putInt("alarm"+alarmNumber+"m", minute);
                editor.apply();
        } else {
            Toast.makeText(getBaseContext(), getString(R.string.maxalarms), Toast.LENGTH_SHORT).show();
        }
    }
}
